// Calcular média de notas
    // 10; 6.5; 8; 7.5
const notas = [10, 6.5, 8]; // lista contendo notas
let media = (notas[0] + notas[1] + notas[2])/notas.length
console.log(media)

// Adicionando elemento em uma array
notas.push(7)
console.log(notas)

//Deletando último elemento em uma array
notas.pop()
console.log(notas)
 
//Outros métodos em array: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array 

// Dividindo arrays - método .slice()
const nomes = ["João", "Juliana", "Ana", "Isabela", "Larissa", "Gustavo", "William", "Andrea"];

const sala1 = nomes.slice(0, nomes.length/2) // 0 - inicio da divisão; nomes.lenght - termino da divisão 
const sala2 = nomes.slice(nomes.length/2) 
console.log(sala1, sala2)

// Alterando arrays - método .splice()
nomes.splice(0, 1, "Leandro") // 0 - inicio da substituição; 1 - número de elementos que serão removidos; "Leandro" - objeto que será inserido
console.log(nomes)

// Concatenação de arrays
const outrosNomes = ["Jakson", "Marcos", "Andre"];
const novosNomes = nomes.concat(outrosNomes);
console.log(novosNomes)

//Lista dentro de listas
const alunos = ["Gustavo", "Isabela", "William", "Larissa"]
const medias = [10, 9, 6, 5];
let mediasAlunos = [alunos, medias];
console.log(mediasAlunos)
console.log(`A média do ${mediasAlunos[0][0]} é ${mediasAlunos[1][0]}`)

// Buscando algo dentro da lista
const buscaAlunoMedia = (aluno) => {
    if (mediasAlunos[0].includes(aluno)) // se na lista "mediasAlunos" tiver o parametro inserido em aluno...
    {
        indice = mediasAlunos[0].indexOf(aluno)// define a busca do aluno inserido no parametro dentro da lista "mediasAlunos"
        return `${mediasAlunos[0][indice]}, sua média é ${mediasAlunos[1][indice]}`
    } else { return `${aluno} não está na lista`}
}
console.log(buscaAlunoMedia("Larissa")) // o parametro aluno é "Larissa"