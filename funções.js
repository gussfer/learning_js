//3 sintaxes para escrever funções
//Declaração de função
function imprimeTexto (texto) {
    console.log(texto)
}
function soma() {
    const res = 2+2;
    console.log(res)
}
imprimeTexto("Hello World");
soma();

// Parâmetros da função - argumentos necessários para excução da função
function somaPlus(num1, num2) {
    return num1 + num2
}
console.log(somaPlus(7,2))

function nomeIdade(nome, idade) { // define o nome dos parametros
    return `O meu nome é ${nome} e minha idade é ${idade}`; // define onde os parametros são inseridos    
}
console.log(nomeIdade('Gus', 25)); // insere os parametros

// Expressão de função - escrita alternativa
const somaExp = function (num1, num2) {return num1 + num2}
console.log(somaExp(1, 1)) 

// Arrow function 
const somaArrow = (num1, num2) => num1 + num2
console.log(somaArrow(1, 1))

const churras = (pessoas) => {
    let valorCarne = ((pessoas*150)/1000)*40;
    let valorFrang = ((pessoas*150)/1000)*25;
    let valorLing = ((pessoas*150)/1000)*18;
    let total = valorCarne + valorFrang + valorLing;
    return `Valor total carnes: ${total.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}`;
}
console.log(churras(50))