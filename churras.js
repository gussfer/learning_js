// function qtdCarne(pessoas) { // define o nome dos parametros
//     let calcCarne = (pessoas*150)/1000;
//     return `Quantidade de carne necessaria: ${calcCarne}kg`; // define onde os parametros são inseridos    
// }
// console.log(qtdCarne(50));

// function qtdLing(pessoas) { // define o nome dos parametros
//     let calcLing = (pessoas*150)/1000;
//     return `Quantidade de linguiça necessaria: ${calcLing}kg`; // define onde os parametros são inseridos    
// }
// console.log(qtdLing(50));

// function qtdFrang(pessoas) { // define o nome dos parametros
//     let calcFrang = (pessoas*150)/1000;
//     return `Quantidade de frango necessaria: ${calcFrang}kg`; // define onde os parametros são inseridos    
// }
// console.log(qtdFrang(50));

// function valorCarne(pessoas) { // define o nome dos parametros
//     let valor = ((pessoas*150)/1000)*40;
//     return `Valor total da carne: ${valor.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}`; // define onde os parametros são inseridos    
// }
// console.log(valorCarne(50));

// function valorLing(pessoas) { // define o nome dos parametros
//     let valor = ((pessoas*150)/1000)*18;
//     return `Valor total da linguiça: ${valor.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}`; // define onde os parametros são inseridos    
// }
// console.log(valorLing(50));

// function valorFrang(pessoas) { // define o nome dos parametros
//     let valor = ((pessoas*150)/1000)*25;
//     return `Valor total da frango: ${valor.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}`; // define onde os parametros são inseridos    
// }
// console.log(valorFrang(50));

// function qtdCerveja(pessoas) {
//     let calcCerveja = Math.round((pessoas*10)/12);
//     return `Quantidade de cerveja necessaria: ${calcCerveja} fardos`
// }
// console.log(qtdCerveja(50));

// function litrosCerveja(pessoas) {
//     let litro = Math.round(pessoas*0.269*10);
//     return `Quantidade de litros de cerveja: ${litro} litros`
// }
// console.log(litrosCerveja(50));

function valorCerveja(pessoas) {
    let valor = pessoas*10*1.99;
    return `Valor total cerveja: ${valor.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}`;
}
console.log(valorCerveja(50));

function valorCarnes(pessoas) {
    let valorCarne = ((pessoas*150)/1000)*40;
    let valorFrang = ((pessoas*150)/1000)*25;
    let valorLing = ((pessoas*150)/1000)*18;
    let total = valorCarne + valorFrang + valorLing;
    return `Valor total carnes: ${total.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}`;
}
console.log(valorCarnes(50));

function gastosCervCarne(pessoas) {
    let valorCerv = pessoas*10*1.99;
    let valorCarne = ((pessoas*150)/1000)*40;
    let valorFrang = ((pessoas*150)/1000)*25;
    let valorLing = ((pessoas*150)/1000)*18;
    let total = valorCerv + valorCarne + valorFrang + valorLing;
    return `Valor total: ${total.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}`;
}
console.log(gastosCervCarne(50))




