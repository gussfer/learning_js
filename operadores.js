// Comparadores \\

// == compara valor
const num = 5;
const numString = "5"
console.log(num == numString)

// // === compara valor e tipo
console.log(num === numString)

// Operador ternário - mesma lógica do if, porém com sintaxe distinta
const idadeMinima = 18;
const idadeCliente = 16;
            // condição                   True          False    
console.log(idadeCliente >= idadeMinima ? "Autorizado" : "Não autorizado")
    // Equivale a:
        /*if (idadeCliente >= idadeMinima) {
            console.log("Autorizado")
        } else {
            console.log("Não autorizado")
        } */

// Template String - `${}`
const nome = 'Gus';
const cidade = 'Maua';
const idade = 2022 - 1996;
const dados = `Meu nome é ${nome} e nasci na cidade de ${cidade}. Atualmente tenho ${idade} anos.`; 
console.log(dados)

