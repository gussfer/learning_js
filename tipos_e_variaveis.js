// Number - algoritmos numericos

const firstNumber = 3;
const secondNumber = 4.5;

const sumNumber = firstNumber + secondNumber;
console.log(sumNumber.toFixed(2)); // método toFixed() arredonda numero para casas decimais informadas
console.log(sumNumber.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})) // metodo retorna valor formatado em formato de real

//String - textos

const texto = 'Hello World';
const text1 = ', Gus';
const textSum = texto + text1;
console.log(textSum)

// Boolean - verdadeiro ou falso
console.log(firstNumber === secondNumber)

// Tipos de Variáveis (var, let e const)

// Var - pode ser chamada em qualquer lugar do código
    // permite alteração
var comprimento = 3;
var largura = 5;
var area = comprimento * largura;
console.log(area)

// Let - somente pode ser chamada dentro do seu escopo de atuação (escopo de bloco), p. ex dentro de um comando if 
    // permite alteração
var exibeMensagem = function() {
    if(true) { 
        var escopoFuncao = 'Caelum'; 
        let escopoBloco = 'Alura';
        console.log(escopoBloco); // executa pois está dentro do if
   } 
   console.log(escopoFuncao); // executa pois pode ser chamada em qualquer lugar do código
   console.log(escopoBloco); // não executa pois está fora do escopo do if
}
exibeMensagem();

// // Const - uma vez declarada, não é alterada durante todo o código; tambem possui escopo de bloco

var exConst = function() {
    if(true) { 
        const mensagem = 'Alura'; 
        mensagem = 'Caelum';
        console.log(mensagem); // não executa pois a const não permite alteração
    }
}
exConst()

// Conversão de tipo: Number() e String()
const numero = 45;
const numString = "45";
console.log(numero + Number(numString))